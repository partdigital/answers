<?php
// Exercise 1: Output 
//--------------------
print "<p><strong>HAMLET</strong><br>
Then is doomsday near: but your news is not true.<br>
Let me question more in particular: what have you,<br>
my good friends, deserved at the hands of fortune,<br>
that she sends you to prison hither?
</p>"; 

print "<p><strong>GUILDENSTERN</strong><br>
Prison, my lord!
</p>"; 

print "<p><strong>HAMLET</strong><br>
Denmark's a prison.</p>"; 

print "<p><strong>ROSENCRANTZ</strong><br>
Then is the world one.</p>"; 

print "<p><strong>HAMLET</strong><br>
A goodly one; in which there are many confines,<br>
wards and dungeons, Denmark being one o' the worst.</p>"; 

print "<p><strong>ROSENCRANTZ</strong><br>
We think not so, my lord.</p>"; 

print "<p><strong>HAMLET</strong><br>
Why, then, 'tis none to you; for there is nothing<br>
either good or bad, but thinking makes it so: to me<br>
it is a prison.</p>"; 

print "<p><strong>ROSENCRANTZ</strong><br>
Why then, your ambition makes it one; 'tis too<br>
narrow for your mind.</p>"; 

// Exercise 2: Variables
//--------------------
$string = 'This is a string';
var_dump($string); 

print '<br>'; 

$integer = 1234; 
var_dump($integer); 

print '<br>'; 

$float = 1.234;
var_dump($float); 

print '<br>'; 

$boolean = TRUE; 
var_dump($boolean); 

print '<br>'; 

$null = NULL; 
var_dump($null); 


// Exercise 3: Mad Libs!
//--------------------
$noun_1 = '';
$noun_2 = '';
$noun_3 = ''; 
$noun_4 = ''; 
$noun_5 = ''; 
$adjective_1 = '';
$adjective_2 = ''; 
$adjective_3 = ''; 
$adjective_4 = ''; 
$adjective_5 = ''; 
$adjective_6 = ''; 
$adjective_7 = ''; 
$exclamation = ''; 
$verb_1 = ''; 
$verb_2 = ''; 
$verb_3 = ''; 

print "Once upon a " . $noun_1 .", there were three little pigs. The<br>
first little pig was very " . $adjective_1 . ", and he built a house for<br>
himself out of " . $noun_2 . " . The second little pig was<br>
" . $adjective_2 .", and he built a house out of " . $noun_3 .". But<br>
the third little pig was very " . $adjective_3 . ", and he built his house<br>
out of genuine " . $adjective_4 . ". Well one day, a mean wolf<br>
came along and saw the houses. \"" . $exclamation . "!\" he said. \"I'll<br>
" . $verb_1 . "and I'll " . $verb_2 ." and I'll blow your house<br>
down.\" And he blew down the first little pig's " . $adjective_5 ." " . $noun_4 . ". and<br>
the second little pig's " . $noun_5 .". The two little pigs ran<br>
to the third pig's house. Thereupon, the wolf began blowing, but he<br>
couldn't blow down the third little pig's " . $adjective_6 ." house. so he<br>
" . $verb_3 ." off into the forest, and the three little " . $adjective_7 ."<br>
pigs moved to Los Angeles and started their own boy band.<br>"; 


// Exercise 4: Math
//--------------------

// Find the area of a square whose sides are 15 x 15 inches. 
$width = 15; 
$height = 15; 
$area = $width * $height; 
print $area; 


// Convert 12.5 miles to kilometers. 
$miles = 12.5; 
$km = $miles * 1.6; 
print $km; 

//Jeremy likes to have a cup of coffee every morning before work. He sometimes also buys a donut. 
//Each week for the last month Jeremy has spent $23.50, $30.12, $18.75 and $40.25. How much has 
//he spent in the last month?  
$week1 = 23.50;
$week2 = 30.12;
$week3 = 18.75; 
$week4 = 40.25;
$total = $week1 + $week2 + $week3 + $week4; 

$string = "A cat is&nbsp;&nbsp;here"; 

// Bonus 
//--------------------
// Math: If the radius of a circle is 5 inches, what is its area? 
$radius = 5; 
$pi = pi(); // or 3.14; 
$area = $pi * pow($radius,2); 
print $area; 

// Write an a array storing your favorite animals. var_dump that array. 
$animals = array('cat', 'dog', 'mouse', 'lizard', 'camel'); 
var_dump($animals); 

// Write an array with values 1 - 20. var_dump that array.  
$numbers = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20); 
var_dump($numbers); 
