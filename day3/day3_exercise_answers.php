<?php
// Exercise 1: Functions and Math
//--------------------
// Write a function that accepts two arguments and calculates the area of a rectangle whose 
// sides are 20 x 5 inches. Call the function and var_dump the final result.  
function sum_ints($int1, $int2) {
	return $int1 + $int2; 
}
$sum = sum_ints(20,5); 
var_dump($sum); 


// Write a function that accepts two years (ex: 2005, 1990) as arguments. Calculate the 
// difference between those two years and return the absolute value. Call the function 
// and var_dump() the final result. 
function diff_years($year1, $year2) {
	$diff = $year1 - $year2; 
	return abs($diff); 
}
$difference = diff_years(2005, 1990); 
var_dump($difference); 


// Exercise 2: Functions and Arrays 
//--------------------
// Write a function that accepts an indexed array of integers as an argument. Calculate 
// and return the sum of values in that array. Call the function and var_dump() the 
// final result. 
function sum_array($array) {
	$sum = 0;
	foreach ($array as $integer) {
		$sum = $sum + $integer;  // Or $sum += $integer; 
	}
	return $sum; 
}
$integers = array(1,5,4,3,6); 
$sum = sum_array($integers); 
var_dump($sum); 

// Refer to the array below. Write a function called ‘array_find’ that accepts two arguments; 
// a string and an array. Inside your function use a loop to iterate through the array, 
// use a conditional to find the value and return the key of that value. For example if you 
// searched for “strawberry” it would return 1. 
$array = array('chocolate', 'strawberry', 'vanilla', 'pistachio'); 

function array_find($needle, $haystack) {
	foreach ($haystack as $key => $value) {
		if ($value == $needle) {
			return $key; 
		}
	}
}

$key = array_find('strawberry', $array); 
var_dump($key); 

// Exercise 3: Random Image Generator
//--------------------
// See image.php 

// Exercise 4: Dyanmic List of Links 
//--------------------
// See home.php 