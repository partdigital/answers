<?php

/**
* Create a link.
*/ 
function hyperlink($text, $url) {
    return '<a href = "' . $url .'">' . $text . '</a>'; 
}

/**
* Generate a list from an array.
*/ 
function create_list($array) {
    
    $list = '<ul>';
    foreach ($array as $url => $text) {
       $list .= '<li>' . hyperlink($text, $url) .'</li>';  
    }
   return $list .= '</ul>';
}

// Links array. 
$links = array(
    'http://www.google.com' => 'Google',
    'http://www.facebook.com' => 'Facebook',
    'http://www.wikipedia.org' => 'Wikipedia',
    'http://www.twitter.com' => 'Twitter',
); 

?>
<!DOCTYPE html>
<html>
<head>
<title>List of Links</title>
</head>
<body>
    <?php print create_list($links); ?>
</body>
</html>