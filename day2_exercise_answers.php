<?php
// Exercise 1: Comparison Operators
//--------------------
$animal = 'cat'; 
if ($animal == 'cat') {
	print 'The animal is a ' . $animal .'<br>'; 
}

$integer = 10; 
if ($integer === '10') {
	print 'The integer is ' . $integer . '<br>'; 
}

$world = 'round'; 
if ($world != 'flat') {
	print 'The world is ' . $world . '<br>'; 
}

$tomato = 'fruit';
if ($tomato <> 'vegetable') {
	print 'Tomato is a ' . $tomato . '<br>'; 
}

$numeric_str = '1.4'; 
if ($numeric_str !== 1.4) {
	print $numeric_str . ' does not equal 1.4 <br>'; 
}

$price = 5.00; 
if ($price < 10.00) {
	print $price . ' is less than 10.00 <br>'; 
}

$integer = 50; 
if ($integer <= 50) {
	print 'The integer is ' . $integer . '<br>'; 
}

$temperature = 99; 
if ($temperature > 75) {
	print 'It is very hot out, the temperature is ' . $temperature .' degrees<br>'; 
}

$score = 80;
if ($score >= 65) {
	print '65% or higher is a passing grade, the student got a score of ' . $score . '<br>';
}

// Exercise 2: Equivalent Conditional Expressions
//--------------------
$var = ''; 

if (100 == $var) {
	//...
}

if (!$var == 50) {
	//...
}

if (250 > $var) {
 	//...
}

// 
// Exercise 3: Multiple Conditions
//--------------------
$animal = 'Cow'; 
if ($animal == 'Dog') {
	print 'Woof'; 
} elseif ($animal == 'Cat') {
	print 'Meow'; 
} elseif ($animal == 'Cow') {
	print 'Moo'; 
} elseif ($animal == 'Sheep') {
	print 'Baa'; 
} elseif ($animal == 'Pig') {
	print 'Oink'; 
} else {
   print 'Oh no! No animals';  
}

// Bonus 
switch ($animal) {
	case 'Dog':
		print 'Woof'; 
		break; 
	case 'Cat':
		print 'Meow';
		break; 
	case 'Cow':
		print 'Moo'; 
		break; 
	case 'Sheep':
		print 'Baa'; 
		break; 
	case 'Pig':
		print 'Oink'; 
		break ;
    default:
         print 'Oh no! no animals';  
        break; 
} 


// Exercise 4: Indexed versus Associative Arrays
//--------------------
// Associative 
// Indexed
// Associative
// Associative
// Indexed
// Indexed 

// Exercise 5: Indexed Arrays
//--------------------
$groceries = array('Bread', 'Milk', 'Cheese', 'Butter', 'Onions', 'Cereal', 'Yogurt', 'Apples', 'Jam', 'Mushrooms', 'Orange Juice', 'Sugar', 'Flour', 'Peanut Butter', 'Bananas');  


// Exercise 6: Associative Arrays 
//--------------------
$states = array(
    'CT' => 'Connecticut',
    'MA' => 'Massachusetts',
    'ME' => 'Maine',
    'NH' => 'New Hampshire',
    'RI' => 'Rhode Island',
    'VT' => 'Vermont'
); 

// Exercise 7: While Loops
//--------------------
$i = 0; 
while ($i < 20) {
	var_dump($i); 
	$i++;  
}

// Exercise 8: Using loops to grab data from an array.
//--------------------
$i = 0; 
$limit = count($groceries); 
while ($i < $limit) {
	var_dump($groceries[$i]); 
	$i++; 
}

// Bonus: Using loops to create an array.
//--------------------
$i = 0; 
$array = array(); 
while ($i < 10) {
	$array[] = $i; 
	$i++; 
}

// Exercise 9: While loops versus For loops versus Foreach loops. 
//--------------------
// While loop
// For loop / Foreach loop
// While loop / For loop
// While loop

// Exercise 10: For Loops
//--------------------
// Write a For loop that iterates 10 times and var_dump() the index ($i). 
for ($i = 0; $i < 10; $i++) {
	var_dump($i); 
}


// Refer to the array below. Write a For loop that iterates through each 
// element and var_dump the value. 
$candy = array('Snickers', 'Mars', 'Twizzlers', 'Butterfinger', 'Skittles', 'KitKat', 'Milky Way');

$limit = count($candy); 
for ($i = 0; $i < $limit; $i++) {
	var_dump($candy[$i]); 
}


// Exercise 11: Foreach Loops
//--------------------
// Refer to the dataset below. Write a Foreach loop that iterates through the dataset 
// and var_dump() both the key and the value. Concatenate the key and value into a single string. 
$sports = array('Hockey', 'Baseball', 'Football', 'Soccer', 'Golf', 'Basketball'); 
foreach ($sports as $sport) {
	var_dump($sport); 
}

$players = array(
	'Matt' => 39,
	'Patrice' => 37,
	'Brett' => 14, 
	'Adam' => 54,
	'Joe' => 45,
	'Zach' => 62
); 

foreach($players as $key => $value) {
	var_dump($key . ' = ' . $value); 
}

// Exercise 12: Equivalent Loops
//--------------------
for ($i = 10; $i < 100; $i++) {
	var_dump($i); 
}

$array = array(1,10,15,30,101); 
foreach ($array as $value) {
	print $value . '<br>'; 
}


// Bonus: Multidimensional Arrays
//--------------------
// Refer to the array below. Write a foreach loop which iterates through each element and 
// var_dump() the name and age fields in a single string. 
$animals = array(
    'cat' => array(
        'name' => 'Fluffy',
        'age' => 9,
    ),     
    'dog' => array(
        'name' => 'Fido',
        'age' => 6 
    ),     
    'pig' => array(
        'name' => 'Babe',
        'age' => 5 
    )     
); 

foreach ($animals as $animal) {
    // $animal = array('name' => 'Fluffy', 'age' => 9); 
	var_dump($animal['name'] . ' = ' . $animal['age']); 
}


